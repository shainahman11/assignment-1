import Axios from "axios"

const $axios = Axios.create({
    baseURL:'/api/',
    headers:{
        'Content-Type':'application/json'
    },
})

//Example of a cross-cutting concern - client api error-handling
$axios.interceptors.response.use(
    (response) => response,
    (error) => {
        console.error("got error")
        console.error(error)

        throw error;
    });

class NumbersService {
    static getNumber(userNumber) {
        return $axios
            .get('numbers/calculate-number',{params:{
                    string: userNumber,
                }})
            .then(response => response.data)
    }
}

const service = {
    NumbersService
}

export default service