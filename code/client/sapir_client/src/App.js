import './App.css';
import service from "./api";
import {useState} from "react";


function App() {
  const [number,set_number] = useState('');
    const [return_number,set_return_number] = useState('');

    const callCalculateNumFuncOnClick = () =>{
        service.NumbersService.getNumber(number).then(response =>{
            set_return_number(response.sum_of_number)
        })
    }

    return (
    <div className="App">
      <header className="App-header">
          <div><b>Sum of Number</b></div><br/>
          <div>Insert Number:
              <input placeholder={"Enter Number"} onChange={event => set_number(event.target.value)}/>
            <br/>
              <output>{return_number}</output>
              <br/>
              <button onClick={callCalculateNumFuncOnClick}>Send</button>
        </div>
      </header>
    </div>
  );
}

export default App;
