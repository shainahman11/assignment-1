from rest_framework import status
from rest_framework.decorators import api_view, renderer_classes
from rest_framework.renderers import JSONRenderer
from rest_framework.response import Response

from sapir_server.models import Numbers
from sapir_server.serializers import NumbersSerializer


@api_view(['GET'])
@renderer_classes([JSONRenderer])
def call_calculate_num(request):
    received_number = (request.GET.get('string', ''))
    sum1 = calculate_num(received_number)
    result = persist_new_number(received_number, sum1)
    serializer = NumbersSerializer(result)
    return Response(serializer.data, status=status.HTTP_200_OK)


def calculate_num(input_number):
    num_of_digits = len(input_number)
    input_number = int(input_number)
    sum1 = 0

    for i in range(num_of_digits):
        sum1 += input_number % 10
        input_number //= 10

    result = str(sum1)
    return result


def persist_new_number(input_number, result):
    new_number = Numbers(sum_of_number=result, number=input_number)
    new_number.save()
    return new_number
