from rest_framework import serializers

from sapir_server.models import Numbers


class NumbersSerializer(serializers.ModelSerializer):
    class Meta:
        model = Numbers
        fields = ['id', 'number', 'sum_of_number', 'creation_date']
