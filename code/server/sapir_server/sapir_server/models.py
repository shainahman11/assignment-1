from django.db import models


class Numbers(models.Model):
    id = models.AutoField(primary_key=True)
    number = models.CharField(max_length=10)
    sum_of_number = models.CharField(max_length=10)
    creation_date = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return '%s' % self.number
